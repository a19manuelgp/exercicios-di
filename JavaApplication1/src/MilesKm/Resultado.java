/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MilesKm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author a19manuelgp
 */
public class Resultado implements ActionListener{
    private PPrincipal pantalla1;
    
    public Resultado(PPrincipal pantalla1){
       this.pantalla1 = pantalla1;
        
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String distanciaTxt = pantalla1.getjTextField1Distancia().getText();
        Float distancia = Float.valueOf(distanciaTxt);
        String unidade = pantalla1.getjChoice().getSelectedItem();
        String udRdo;
        float rdo;
        if (unidade.equalsIgnoreCase("Miles")){
            rdo = distancia * 1.609f;
            udRdo="KM";
        }
        else{
            rdo = distancia/1.609f;
            udRdo="Mls";

        }
        
        String rdoString = String.valueOf(rdo);
                
        pantalla1.estabelcerResultado(rdoString + " " + udRdo);
        
    }
    
}
